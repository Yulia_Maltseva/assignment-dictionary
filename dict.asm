global find_word
extern string_equals

%include "colon.inc"

global _start

section .text

find_word:
					; rdi - адрес нуль-терм. имени
					; rsi - адрес последнего слова
					; Адрес, если найдено, иначе rax = 0
mov r8, rdi									
.loop:
	mov r9, rsi
	cmp rsi, 0 			; Проверяем, не пустой ли список слов
	je .exit_false 		; Выйдем и скажем, что ничего не нашли
	mov rdi, r8			; Положим строку в rdi
	add rsi, 8			; Смотрим что там за строка
	call string_equals	; Сравниваем строки
	mov rsi, r9			; Возвращаем rsi на место (после string_equals укзатель на конец строки)
	cmp rax, 0 			; Проверяем, что вернула функция
	jnz .exit_true		; Если не 0 - слова совпали
	mov rsi, [rsi]		; Иначе смотрим следующее слово
    jmp .loop
	
.exit_true:				; Если нашли строку - сохраняем указатель на нее
	mov rax, rsi
	ret 
	
.exit_false:
	mov rax, 0			; Если не нашли - сохраняем 0
	ret