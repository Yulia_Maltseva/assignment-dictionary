%define BUFFER_SIZE 256
%define stdout 1
%define stderr 2

section .data
	msg: db "No word", 0

global _start	
	
section .text

%include "words.inc"

extern read_word
extern find_word
extern string_length
extern print_string
extern print_newline

_start: 
	
	mov rsi, BUFFER_SIZE - 1	; Максимальная длина слова, что мы можем записать в буфер
	sub rsp, BUFFER_SIZE	; Освободим место под буфер в стеке
	
	mov rdi, rsp	; Передадим вершину буфера в read_word
	call read_word	; Начало слова - в rax
	
	cmp rax, 0		; Проверяем, не 0 ли там (есть ли слово)
	je .false
	 
	mov rdi, rax	; Сохраним слово в rdi
	mov rsi, next	; Адрес посл. слва - в rsi
	call find_word	; Делаем поиск - возвращает указатель на строку
	cmp rax, 0		; Проверка на ноль, установка флагов
	je .false		; Сюда, если слово не найдено
	add rax, 8		; Указатель на начало строки, а не на адрес следующего слова	
	mov r10, rax 	; Сохраняем значение rax
	mov rdi, rax	; Передаем rax как аргкмент для string_length
					; Дальше нам надо найти строку по метке 
					;(Для этого надо найти сначала метку, которая лежит после строки)
		 						
	call string_length
	add r10, rax	; Сдвинем указатель на начало метки
	inc r10			; Учитываем нуль-терминатор
	mov rdi, r10	; Ссылка на слово - в rdi
	
	mov r13, stdout		; Дескриптор для stdout
	call print_string
	
.exit:	
	call print_newline
	mov rax, 60 	;
	mov rdi, rax 	; Чтобы echo выводило то, что у нас возвращает find_word
	syscall
	ret
	
.false:
	mov rdi, msg	; Если нет слова, сообщаем
	call string_length
	mov rsi, rax
	
	mov r13, stderr		; Дескриптор для ошибок
	call print_string
	jmp .exit
